/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SPEEDOMETER_H
#define SPEEDOMETER_H

#include "../luckey.h"

class Urho;

class Speedometer: public LogicComponent
{
    DRY_OBJECT(Speedometer, LogicComponent);

public:
    Speedometer(Context* context);

    void Update(float timeStep) override;
    void SetUrhoTwo();

protected:
    void OnNodeSet(Node* node) override;

private:
    void SubscribeToEvents();
    Urho* GetUrho();
    Color AccelerationToColor(float acceleration);

    void HandleUrhoSwam(StringHash eventType, VariantMap& eventData);
    void HandleUrhoHit(StringHash eventType, VariantMap& eventData);

    bool urhoOne_;
    Pair<SharedPtr<Material>, SharedPtr<Material>> glowMats_;
};

#endif // SPEEDOMETER_H
