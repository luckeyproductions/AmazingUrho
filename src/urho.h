/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef URHO_H
#define URHO_H

#include "sceneobject.h"

#define SPEED_MIN 1.23f

class Urho: public SceneObject
{
    DRY_OBJECT(Urho, SceneObject);

public:
    static const PODVector<Urho*> GetUrhos() { return { urhos_.first_, urhos_.second_ }; }
    static Urho* GetUrho(bool one = true) { return (one ? urhos_.first_ : urhos_.second_); }
    static void ResetPointers() { urhos_.first_ = urhos_.second_ = nullptr; }

    static float DistanceToAcceleration(float d) { return Pow(1.42f * (1.f - Abs(d - .23f)), 3.f); }

    Urho(Context* context);
    void OnNodeSet(Node* node) override;
    void Update(float timeStep) override;

    float GetSpeed() const { return speed_; }
    float GetDistance() const { return node_->GetWorldPosition().DistanceToPoint(targetPosition_); }
    void Set(const Vector3& position, const Quaternion& rotation) override;
    void Hit();

    void Jump(const Vector3& shift);

private:
    static Pair<Urho*, Urho*> urhos_;

    Vector3 startPosition_;
    Quaternion startRotation_;
    Vector3 targetPosition_;
    float speed_;

    Node* graphicsNode_;
    Node* offsetNode_;
};

DRY_EVENT(E_URHOSWAM, UrhoSwam)
{
    DRY_PARAM(P_FISH, Fish);        // ptr
    DRY_PARAM(P_SUCCESS, Success);  // bool
}

DRY_EVENT(E_URHOHIT, UrhoHit)
{
    DRY_PARAM(P_FISH, Fish);        // ptr
}

#endif // URHO_H
