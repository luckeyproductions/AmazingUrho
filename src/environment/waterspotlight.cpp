/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "waterspotlight.h"

WaterSpotLight::WaterSpotLight(Context* context) : LogicComponent(context),
    first_{false}
{
}

void WaterSpotLight::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->SetPosition(Vector3::UP * 17.f);
    Light* spotLight{ node_->CreateComponent<Light>() };
    spotLight->SetLightType(LIGHT_SPOT);
    spotLight->SetFov(123.f);
    spotLight->SetRange(42.f);
    spotLight->SetBrightness(.34f);
    spotLight->SetShapeTexture(static_cast<Texture*>(CACHE->GetResource<Texture2D>("Textures/Water.png")));
}
void WaterSpotLight::Set(bool first)
{
    first_ = first;
    node_->LookAt(Vector3::DOWN, first_ ? Vector3::FORWARD : Vector3::RIGHT);
    node_->Translate((first_ ? Vector3::RIGHT : Vector3::LEFT) * 5.f, TS_WORLD);
}

void WaterSpotLight::Update(float timeStep)
{
    node_->Rotate(Quaternion(timeStep * (first_ ? 2.f : -3.f), Vector3::UP), TS_WORLD);
    node_->GetComponent<Light>()->SetBrightness(MC->Sine(.23f, .05f, .34f, first_ * .5f));
}



