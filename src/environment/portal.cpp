/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../urho.h"
#include "../spawnmaster.h"
#include "weed.h"

#include "portal.h"

Portal::Portal(Context* context): SceneObject(context),
    other_{ nullptr },
    layer_{ 0 },
    ignore_{}
{
}

void Portal::OnNodeSet(Node* node)
{
    if (!node)
        return;

    SceneObject::OnNodeSet(node);

    StaticModel* model{ node_->CreateComponent<StaticModel>() };
    model->SetModel(RES(Model, "Models/Portal.mdl"));
    model->SetMaterial(RES(Material, "Materials/VCol.xml"));
}

void Portal::Set(const Vector3& position, const Quaternion &rotation)
{
    SceneObject::Set(position, rotation);

    other_ = nullptr;
    layer_ = 0;
    ignore_.Clear();

    SpawnMaster* spawn{ GetSubsystem<SpawnMaster>() };
    for (int w{ 0 }; w < 200; ++w)
    {
        const float offCenter{ 3/4.f - w / 420.f };
        Weed* weed{ spawn->Create<Weed>() };
        weed->Set(node_->GetWorldPosition() + Vector3{ RandomOffCenter(offCenter), Random(.5f, .75f), RandomOffCenter(offCenter) } );
        Node* weedNode{ weed->GetNode() };
        weedNode->SetScale(Random(.875f, 2.3f));
//        weedNode->SetParent(node_);
    }
}

void Portal::Match(int layer)
{
    layer_ = layer;

    PODVector<Portal*> portals{};
    GetNode()->GetParent()->GetComponents<Portal>(portals, true);

    for (Portal* p: portals)
    {
        if (p->GetLayer() == layer_ && p != this)
        {
            other_ = p;
            other_->SetOther(this);
            return;
        }
    }
}

Vector3 Portal::GetOffset(Urho* urho) const
{
    const Vector3 thisPos{ node_->GetWorldPosition() };
    return (IsIgnoring(urho) ? Vector3::ZERO
                             : other_->GetNode()->GetWorldPosition() - thisPos);
}

void Portal::Update(float /*timeStep*/)
{
    if (!other_)
        return;

    for (Urho* urho: Urho::GetUrhos())
    {
        if (!urho)
            continue;

        const Vector3 thisPos{ node_->GetWorldPosition() };
        const Vector3 urhoPos{ urho->GetWorldPosition() };
        const Vector3 urhoDelta{ (thisPos - urhoPos).ProjectOntoPlane(Vector3::UP) };

        if (urhoDelta.Length() < .25f)
        {
            if (!IsIgnoring(urho))
            {
                const Vector3 urhoDir{ urho->GetNode()->GetWorldDirection() };
                if (urhoDir.DotProduct(urhoDelta) > 0.f)
                {
                    urho->Jump(GetOffset(urho));
                    other_->EnableIgnore(urho);
                }
            }
        }
        else if (urhoDelta.Length() > .8f)
        {
            ignore_.Erase(urho);
        }
    }
}
