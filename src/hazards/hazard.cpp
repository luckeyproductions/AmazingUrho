/* A-Mazing Urho
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../urho.h"

#include "hazard.h"

Hazard::Hazard(Context* context): SceneObject(context),
    radius_{ .666f }
{
}

void Hazard::OnNodeSet(Node* node)
{
    if (!node)
        return;

    SceneObject::OnNodeSet(node);
}

void Hazard::Update(float /*timeStep*/)
{
    for (Urho* urho: Urho::GetUrhos())
    {
        if (!urho)
            continue;

        if (hitsUrho(urho))
        {
            PlaySample(CACHE->GetResource<Sound>("Samples/Hit.ogg"), .8f);
            urho->Hit();
        }
    }
}

bool Hazard::hitsUrho(Urho* urho)
{
    return (urho->GetWorldPosition() - node_->GetWorldPosition()).Length() < radius_;
}
