include(src/AmazingUrho.pri)

TARGET = amazingurho

LIBS += \
    $${PWD}/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

DISTFILES += \
    LICENSE_TEMPLATE \
    README.md

unix
{
    isEmpty(DATADIR) DATADIR = $$(XDG_DATA_HOME)
    isEmpty(DATADIR) DATADIR = $$(HOME)/.local/share

    target.path = /usr/games/
    INSTALLS += target

    resources.path = $$DATADIR/luckey/amazingurho/
    resources.files = Resources/*
    INSTALLS += resources

    icon.path = $$DATADIR/icons/
    icon.files = amazingurho.png
    INSTALLS += icon

    desktop.path = $$DATADIR/applications/
    desktop.files = amazingurho.desktop
    INSTALLS += desktop

    DEFINES += RESOURCEPATH=\\\"$${resources.path}\\\"
}
